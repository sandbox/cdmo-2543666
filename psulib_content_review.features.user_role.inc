<?php
/**
 * @file
 * psulib_content_review.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function psulib_content_review_user_default_roles() {
  $roles = array();

  // Exported role: author.
  $roles['author'] = array(
    'name' => 'author',
    'weight' => 4,
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 3,
  );

  return $roles;
}
