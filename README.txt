INTRODUCTION
------------
The No Workflow module is a "Feature module" that builds a simple after-the-
fact content review system.  The aim of the module is to let your editors do
their work and review their changes after they've been published.  Rather than
sticking author submissions in a queue that doesn't get published until an
editor reviews it and publishes, trust your authors to write good content and
address any problems after-the-fact.  Eliminate editorial bottlenecks!

* To submit bug reports and feature suggestions, or to track changes:
https://github.com/cdmo/no_workflow/issues

* This is a beta project with many todos.  I wouldn't recommend rolling it into your production site and I am not liable for any problems you might encounter if you do so.

CONFIGURATION
-------------
This module consists of a basic Rule to set a "Needs Review" field to true.
Whenever an "author" makes a change to a node with the "Needs Review" field on
it, the Rule is automatically set to true. When that field is true on a given
node of content, a View sees it. "Editors" will see this View but "authors"
will not.

What you have to do:

* Make your content types
* Any content types you want your editors to review, add the "Need Review"
field
* Set the Block for the View to user and user/* and restrict it to authenticated users

REQUIREMENTS
------------
Along with the modules listed in the .info file, this Feature assumes your
starting with a blank site.  If you aren't you just have to be sure there
aren't going to be collisions with settings, especially role names (author and
editor).
