<?php
/**
 * @file
 * psulib_content_review.features.inc
 */

/**
 * Implements hook_views_api().
 */
function psulib_content_review_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
