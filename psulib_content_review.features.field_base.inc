<?php
/**
 * @file
 * psulib_content_review.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function psulib_content_review_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_needs_review'
  $field_bases['field_needs_review'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_needs_review',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Reviewed',
        1 => 'Needs Review',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  return $field_bases;
}
