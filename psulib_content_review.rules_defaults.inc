<?php
/**
 * @file
 * psulib_content_review.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function psulib_content_review_default_rules_configuration() {
  $items = array();
  $items['rules_mark_content_as_needing_review'] = entity_import('rules_config', '{ "rules_mark_content_as_needing_review" : {
      "LABEL" : "Mark content as needing review",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "node" ], "field" : "field_needs_review" } },
        { "NOT user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "3" : "3" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-needs-review" ], "value" : "1" } }
      ]
    }
  }');
  return $items;
}
